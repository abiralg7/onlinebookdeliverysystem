﻿using eBookDelivery.Data.Services;
using eBookDelivery.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Controllers
{
    public class PublisherController : Controller
    {
        private readonly IPublisherService _service;
        public PublisherController(IPublisherService service)
        {
            _service = service;
        }
        public IActionResult Index()
        {
            return View(_service.GetAll());
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create([Bind("FullName,ProfilePicture,Address,EmailAddress")] Publisher publisher)
        {
            if (!ModelState.IsValid)
            {
                return View(publisher);
            }
            await _service.CreateAsync(publisher);
            return RedirectToAction(nameof(Index));

        }
        public IActionResult Details(int id)
        {
            var userDetails = _service.Details(id);
            if (userDetails == null)
            {
                return View("Not Found");
            }
            return View(userDetails);
        }
        public IActionResult Delete(int id)
        {
            var userDetails = _service.Details(id);
            if (userDetails == null)
            {
                return View("Not Found");
            }
            return View(userDetails);
        }
        public async Task<IActionResult> Deleted(int id)
        {
            await _service.Delete(id);
            return RedirectToAction(nameof(Index));
        }
        public IActionResult Update(int id)
        {
            var userDetails = _service.Details(id);
            if (userDetails == null)
            {
                return View("Not Found");
            }
            return View(userDetails);
        }
        [HttpPost]
        public async Task<IActionResult> Update(Publisher publisher)
        {
            if (!ModelState.IsValid)
            {
                return View(publisher);
            }
            await _service.Update(publisher);
            return RedirectToAction(nameof(Index));
        }
    }
}
