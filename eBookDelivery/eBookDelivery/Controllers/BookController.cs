﻿using eBookDelivery.Data;
using eBookDelivery.Data.Services;
using eBookDelivery.Data.ViewModel;
using eBookDelivery.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookService _service;
        public BookController(IBookService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View(_service.GetAllAsync());
        }

        public async Task<IActionResult> Create()
        {
            var bookDropDownData = await _service.GetDropDownValueAsync();
            ViewBag.Publishers = new SelectList(bookDropDownData.Publishers, "Id", "FullName");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create([Bind("Name,Author,Price,BookPicture,Avilable,BookCategory,Description,PublisherId")] Book book)
        {
            if (!ModelState.IsValid)
            {
                var bookDropDownData = await _service.GetDropDownValueAsync();
                ViewBag.Publishers = new SelectList(bookDropDownData.Publishers, "Id", "FullName");
                return View(book);
            }
            await _service.CreateAsync(book);
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Update(int id)
        {
            var bookDetails = await _service.GetByIdAsync(id);
            var bookDropDownData = await _service.GetDropDownValueAsync();
            ViewBag.Publishers = new SelectList(bookDropDownData.Publishers, "Id", "FullName");
            return View(bookDetails);
        }
        [HttpPost]
        public async Task<IActionResult> Update(Book book)
        {
            if (!ModelState.IsValid)
            {
                var bookDropDownData = await _service.GetDropDownValueAsync();
                ViewBag.Publishers = new SelectList(bookDropDownData.Publishers, "Id", "FullName");
                return View(book);
            }
            await _service.UpdateAsync(book);
            return RedirectToAction(nameof(Index));
        }
        public IActionResult Details(int id)
        {
           var bookDetails = _service.Details(id);
            if(bookDetails == null)
            {
                return View("Not Found");
            }
            return View(bookDetails);
        }
    }
}
