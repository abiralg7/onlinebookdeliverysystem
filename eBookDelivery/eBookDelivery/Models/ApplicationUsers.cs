﻿using eBookDelivery.Data.Enum;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Models
{
    public class ApplicationUsers : IdentityUser
    {
        [Display(Name = "Full name ")]
        [Required(ErrorMessage = "Full name is required")]
        public string FullName { get; set; }
        public UserType UserType { get; set; }
    }
}
