﻿using eBookDelivery.Data.Enum;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Book name is required")]
        [Display(Name = "Book Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Author is required")]
        [Display(Name = "Author name")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [Display(Name = "Enter price")]
        public double Price { get; set; }
        public string BookPictureUrl { get; set; }

        [Required(ErrorMessage = "Book picture is required")]
        [Display(Name = "Book Picture")]
        [NotMapped]
        public IFormFile BookPicture { get; set; }

        [Display(Name = "Avilable")]
        public Avilable Avilable { get; set; }

        [Required(ErrorMessage = "BookCategory is required")]
        [Display(Name = "Select book category")]
        public BookCategory BookCategory { get; set; }

        //Relationship
        public int PublisherId { get; set; }
        [ForeignKey("PublisherId")]
        public Publisher Publisher { get; set; }
    }
}
