﻿using eBookDelivery.Data.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Data.ViewModel
{
    public class RegisterByAdminVM : RegisterVM
    {
        [Display(Name = "User type")]
        [Required(ErrorMessage = "User type is required")]
        public UserType UserType { get; set; }
    }
}
