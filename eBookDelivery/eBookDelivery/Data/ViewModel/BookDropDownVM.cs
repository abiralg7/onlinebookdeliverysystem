﻿using eBookDelivery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Data.ViewModel
{
    public class BookDropDownVM
    {
        public BookDropDownVM()
        {
            Publishers = new List<Publisher>();
        }

        public List<Publisher> Publishers { get; set; }
    }
}
