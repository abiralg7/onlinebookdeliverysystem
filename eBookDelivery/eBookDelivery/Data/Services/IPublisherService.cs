﻿using eBookDelivery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Data.Services
{
    public interface IPublisherService
    {
        List<Publisher> GetAll();
        Publisher Details(int id);
        Task Update(Publisher publisher);
        Task CreateAsync(Publisher publisher);
        Task GetById(int id);
        Task Delete(int id);
    }
}
