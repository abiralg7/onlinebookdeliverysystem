﻿using eBookDelivery.Data.ViewModel;
using eBookDelivery.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Data.Services
{
    public class BookServices : IBookService
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public BookServices(AppDbContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }

        public string UploadedFile(Book data)
        {
            string uniqueFileName = null;
            if (data.BookPicture != null)
            {
                string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/books");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + data.BookPicture.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    data.BookPicture.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }

        public async Task<BookDropDownVM> GetDropDownValueAsync()
        {
            var response = new BookDropDownVM()
            {
                Publishers = await _context.Publishers.OrderBy(n => n.FullName).ToListAsync()
            };
            return response;
        }

        public async Task CreateAsync(Book book)
        {
            var uniqueName = UploadedFile(book);
            book.BookPictureUrl = uniqueName;
            await _context.AddAsync(book);
            await _context.SaveChangesAsync();
        }

        public Book Details(int id)
        {
            var bookDetails =  _context.Books.Include(n => n.Publisher).FirstOrDefault(n => n.Id == n.Id);
            return bookDetails;
        }

        public List<Book> GetAllAsync()
        {
            var data = _context.Books.Include(p => p.Publisher).ToList();
            return data;
        }

        public async Task<Book> GetByIdAsync(int id)
        {
            var bookDetails = await _context.Books.Include(p => p.Publisher).FirstOrDefaultAsync(n => n.Id == id);
            return bookDetails;

        }

        public async Task UpdateAsync(Book book)
        {
            var uniqueFileName = UploadedFile(book);
            book.BookPictureUrl = uniqueFileName;
            _context.Update(book);
            await _context.SaveChangesAsync();
        }
    }
}
