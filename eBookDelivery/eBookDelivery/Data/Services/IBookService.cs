﻿using eBookDelivery.Data.ViewModel;
using eBookDelivery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Data.Services
{
    public interface IBookService
    {
        List<Book> GetAllAsync();
        Task<Book> GetByIdAsync(int id);
        Task UpdateAsync(Book book);
        Book Details(int id);
        Task CreateAsync(Book book);
        Task<BookDropDownVM> GetDropDownValueAsync();
    }
}
