﻿using eBookDelivery.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Data.Services
{
    public class PublisherService : IPublisherService
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public PublisherService(AppDbContext context, IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }

        public string UploadedFile(Publisher data)
        {
            string uniqueFileName = null;
            if (data.ProfilePicture != null)
            {
                string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/publishers");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + data.ProfilePicture.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    data.ProfilePicture.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }

        public async Task CreateAsync(Publisher publisher)
        {
            var uniqueFileName = UploadedFile(publisher);
            publisher.ProfilePictureURL = uniqueFileName;
            await _context.AddAsync(publisher);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var userDetails = await _context.Publishers.FindAsync(id);
            _context.Remove(userDetails);
            await _context.SaveChangesAsync();
        }

        public Publisher Details(int id)
        {
            var userDetails = _context.Publishers.Find(id);
            return userDetails;
        }

        public List<Publisher> GetAll()
        {
            var data = _context.Publishers.ToList();
            return data;
        }

        public async Task GetById(int id)
        {
            await _context.Publishers.FindAsync(id);
        }

        public async Task Update(Publisher publisher)
        {
            var uniqueFileName = UploadedFile(publisher);
            publisher.ProfilePictureURL = uniqueFileName;
            _context.Update(publisher);
            await _context.SaveChangesAsync();
        }
    }
}
