﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Data.Enum
{
    public enum UserType
    {
        Admin = 1,
        User,
        Delivery
    }
}
