﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eBookDelivery.Data.Enum
{
    public enum BookCategory
    {
        Adventure = 1,
        Action,
        Horror,
        Documentary,
        Comedy,
        Anime
    }
}
